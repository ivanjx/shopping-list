const electron = require('electron');
const url = require('url');
const path = require('path');

// When on production.
process.env.NODE_ENV = 'production';

// Getting electron.
const {app, BrowserWindow, Menu, ipcMain} = electron;

let mainWindow;

// Creating menu template.
const mainMenuTemplate = [
    {
        label: 'File',
        submenu: [
            {
                label: 'Add item',
                click() {
                    createAddWindow();
                }
            },
            {
                label: 'Clear items',
                click() {
                    mainWindow.webContents.send('item:clear');
                }
            },
            {
                label: 'Exit',
                accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
                click() {
                    app.quit();
                }
            }
        ]
    }
];

// Mac is a damn thing.
// Fixing menu bar.
if(process.platform == 'darwin') {
    mainMenuTemplate.unshift({});
}

// Debugging mode.
if(process.env.NODE_ENV !== 'production') {
    mainMenuTemplate.push({
        label: 'Developer Tools',
        submenu: [
            {
                label: 'Toggle DevTools',
                accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
                click(item, focusedWindow) {
                    focusedWindow.toggleDevTools();
                }
            },
            {
                role: 'reload'
            }
        ]
    });
}

// Create 'addWindow'
let createAddWindow = function() {
    // Create add window.
    addWindow = new BrowserWindow({
        width: 300,
        height: 200,
        title: 'Add a new item'
    });

    // Loading html content.
    addWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'addWindow.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Window close event.
    addWindow.on('close', function() {
        addWindow = null; // This will release the memory used.
    })
};

// Catching item:add.
ipcMain.on('item:add', function(e, itemName) {
    // Sending back to mainWindow.
    mainWindow.webContents.send('item:add', itemName);

    // Done.
    addWindow.close();
})

app.on('ready', function() {
    // Create main window.
    mainWindow = new BrowserWindow({});

    // Loading html content.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'mainWindow.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Quit handler.
    mainWindow.on('closed', function() {
        app.quit();
    })

    // Creating menu.
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);

    // Inserting menu.
    Menu.setApplicationMenu(mainMenu);
});